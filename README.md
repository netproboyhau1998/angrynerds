# AngryNerds
# Project using springboot, lombok

# URL site map
- doccuments/ComputerShop.xlsx
# Configuration file
- src/main/resources/application.properties
# Datatabase (utf8mb4_unicode_ci)
- name default: "computershop". To config go to application.properties file
- DB script in foler DATABASE (including script and diagram.PNG)
# JSP files
- webapp/WEB-INF/views
- static files: src/main/resources/static
